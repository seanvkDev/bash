#
# ~/.bash_profile
#

# Clear proxy-flag
if [ -e "/var/tmp/proxy-mode" ]; then
	rm -f /var/tmp/proxy-mode
fi

# Clear mbsync-lock
if [ -e "/var/tmp/mbsync-lock" ]; then
	rm -f /var/tmp/mbsync-lock
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc

