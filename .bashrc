# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac


# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
# history options
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s histappend
shopt -s hostcomplete
shopt -s nocaseglob

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTCONTROL=ignoredups:erasedups
export PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    #alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# enable sudo completion
complete -cf sudo

# User specific interactive declarations and functions

#misc
BROWSER=/usr/bin/xdg-open
xhost +local:root > /dev/null 2>&1

#-----------
# Searching:
#-----------
# ff:  to find a file under the current directory
ffl () { /usr/bin/find -L . -name "$@" ; }
# ffs: to find a file whose name starts with a given string
ffs () { /usr/bin/find -L . -name "$@"'*' ; }
# ffe: to find a file whose name ends with a given string
ffe () { /usr/bin/find -L . -name '*'"$@" ; }

# grepfind: to grep through files found by find, e.g. grepf pattern '*.c'
# note that 'grep -r pattern dir_name' is an alternative if want all files
grepfind () { find -L . -type f -name "$2" -print0 | xargs -0 grep "$1" ; }
# I often can't recall what I named this alias, so make it work either way:
alias findgrep='grepfind'

# grepincl: to grep through the /usr/include directory
grepincl () { (cd /usr/include; find . -type f -name '*.h' -print0 | xargs -0 grep "$1" ) ; }

# Find a file with a pattern in name:
function ff() { find -L . -type f -iname '*'$*'*' -ls ; }

# Find a directory with pattern $1 in name and Execute $2 on it:
function fde()
{ find -L . -type d -iname '*'${1:-}'*' -exec ${2:-file} {} \;  ; }

# Find a file with pattern $1 in name and Execute $2 on it:
function fe()
{ find -L . -type f -iname '*'${1:-}'*' -exec ${2:-file} {} \;  ; }

# Find a pattern in a set of files and highlight them:
# (needs a recent version of egrep)
function fstr()
{
    OPTIND=1
    local case=""
    local usage="fstr: find string in files.
Usage: fstr [-i] \"pattern\" [\"filename pattern\"] "
    while getopts :it opt
    do
        case "$opt" in
        i) case="-i " ;;
        *) echo "$usage"; return;;
        esac
    done
    shift $(( $OPTIND - 1 ))
    if [ "$#" -lt 1 ]; then
        echo "$usage"
        return;
    fi
    find -L . -type f -name "${2:-*}" -print0 | \
    xargs -0 egrep --color=always -sn ${case} "$1" 2>&- | more

}

function cuttail() # cut last n lines in file, 10 by default
{
    nlines=${2:-10}
    sed -n -e :a -e "1,${nlines}!{P;N;D;};N;ba" $1
}

function lowercase()  # move filenames to lowercase
{
    for file ; do
        filename=${file##*/}
        case "$filename" in
        */*) dirname==${file%/*} ;;
        *) dirname=.;;
        esac
        nf=$(echo $filename | tr A-Z a-z)
        newname="${dirname}/${nf}"
        if [ "$nf" != "$filename" ]; then
            mv "$file" "$newname"
            echo "lowercase: $file --> $newname" else
            echo "lowercase: $file not changed."
        fi
    done
}


function swap()  # Swap 2 filenames around, if they exist
{                #(from Uzi's bashrc).
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
    [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
    [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

function extract()      # Handy Extract Program.
{
     if [ -f $1 ] ; then
         case $1 in
             *.tar.bz2)   tar xvjf $1     ;;
             *.tar.gz)    tar xvzf $1     ;;
             *.bz2)       bunzip2 $1      ;;
             *.rar)       unrar x $1      ;;
             *.gz)        gunzip $1       ;;
             *.tar)       tar xvf $1      ;;
             *.tbz2)      tar xvjf $1     ;;
             *.tgz)       tar xvzf $1     ;;
             *.zip)       unzip $1        ;;
             *.Z)         uncompress $1   ;;
             *.7z)        7z x $1         ;;
             *)           echo "'$1' cannot be extracted via >extract<" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}
#
# Example usage:
#
# - if on master: gbin branch1 <-- this will show you what's in branch1 and
# not in master
# - if on master: gbout branch1 <-- this will show you what's in master that's
# not in branch1
#
# used below
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
# Git branch incoming
function gbin {
    echo branch \($1\) has these commits and \($(parse_git_branch)\) does not
    git log ..$1 --no-merges --format='%h | Author:%an | Date:%ad | %s' --date=local
}
# Git branch outgoing
function gbout {
    echo branch \($(parse_git_branch)\) has these commits and \($1\) does not
    git log $1.. --no-merges --format='%h | Author:%an | Date:%ad | %s' --date=local
}

# Make with colors
make-colors()
{
  pathpat="(/[^/]*)+:[0-9]+"
  ccred=$(echo -e "\033[0;31m")
  ccyellow=$(echo -e "\033[0;33m")
  ccend=$(echo -e "\033[0m")
  /usr/bin/make "$@" 2>&1 | sed -E -e "/[Ee]rror[: ]/ s%$pathpat%$ccred&$ccend%g" -e "/[Ww]arning[: ]/ s%$pathpat%$ccyellow&$ccend%g"
  return ${PIPESTATUS[0]}
}

# set PATH so it includes user's private bin if it exists
[[ -d ~/bin ]] && export PATH=~/bin:$PATH
[[ -d ~/.local/bin ]] && export PATH=~/.local/bin:$PATH

#eval ssh keychain for development
eval `keychain --eval ~/.ssh/id_rsa`

#Xresources
[ -n "${DISPLAY}" ] && eval `/usr/bin/xrdb -merge ~/.Xresources`

#colorgcc
[[ -d /usr/lib/colorgcc/bin ]] && export PATH="/usr/lib/colorgcc/bin:$PATH"

function crosdev() {
	export PATH=/data/cros/depot_tools:/bin:/sbin:/usr/sbin:$PATH
}

#proxy functions
function proxyon(){
	GIT_PROXY_COMMAND="/usr/bin/socks-gateway"
	ftp_proxy="http://proxy-us.intel.com:911"
	http_proxy="http://proxy-us.intel.com:911"
	https_proxy="http://proxy-us.intel.com:911"
	no_proxy="intel.com,*.intel.com,10.0.0.0/8,172.16.0.0/20,192.168.0.0/16,127.0.0.0/8,localhost"
	socks_proxy="http://proxy-us.intel.com:1080"
	export GIT_PROXY_COMMAND http_proxy https_proxy no_proxy socks_proxy ftp_proxy
     	echo -e "\nProxy environment variable set."
 }
 function proxyoff(){
	unset GIT_PROXY_COMMAND
	unset ftp_proxy
	unset http_proxy
	unset https_proxy
	unset socks_proxy
	echo -e "\nProxy environment variable removed."
 }
function sysproxyon(){
	proxyon;
	touch /var/tmp/proxy-mode
}
function sysproxyoff(){
	proxyoff;
	rm -f /var/tmp/proxy-mode
}

#Arch package commands
getpkg () {
	if [[ -z "$1" ]]
	then
		echo "Supply a package name and try again."
	else
		cd /tmp
		[[ -d "/tmp/packages/$1" ]] && rm -rf "/tmp/packages/$1"
		svn checkout --depth=empty svn://svn.archlinux.org/packages && cd packages
		svn update "$1" && cd "$1"
	fi
}


getpkgc () {
	if [[ -z "$1" ]]
	then
		echo "Supply a package name and try again."
	else
		cd /scratch
		[[ -d "/tmp/packages/$1" ]] && rm -rf "/tmp/packages/$1"
		svn checkout --depth=empty svn://svn.archlinux.org/community && cd community
		svn update "$1" && cd "$1"
	fi
}

m_abook_query()
{
    ABOOK=/usr/bin/abook

    if [ -x "$ABOOK" ]          # check whether abook in installed
    then
        for book in ${ABOOK_FILES:-$HOME/.abook/addressbook $HOME/.abook.addressbook}
        do
            if [ -f "$book" ]
            then
                $ABOOK --datafile $book --mutt-query "$@" \
                | sed -e '1d;s/$/       abook/'
            fi
        done
    fi
}

# To speed up builds
export USE_CCACHE=1
export CCACHE_COMPILERCHECK=none
export CCACHE_SLOPPINESS=time_macros,include_file_mtime,file_macro
export CCACHE_DIR='/home/seanvk/.ccache/'

# Some misc settings
export ALTERNATE_EDITOR="vim"
export EDITOR="vim"
export VISUAL="vim"

# Make sure pkg-config can find self-compiled software
# and libraries (installed to ~/usr)
if [ -n $PKG_CONFIG_PATH ]; then
  PKG_CONFIG_PATH=~/usr/lib/pkgconfig
else
  PKG_CONFIG_PATH=$PKG_CONFIG_PATH:~/usr/lib/pkgconfig
fi
export PKG_CONFIG_PATH

# Add custom compiled libraries to library search path.
if [ -n $LD_LIBRARY_PATH ]; then
  LD_LIBRARY_PATH=~/usr/lib
else
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/usr/lib
fi
export LD_LIBRARY_PATH

# Add custom compiled libraries to library run path.
if [ -n $LD_RUN_PATH ]; then
  LD_RUN_PATH=~/usr/lib
else
  LD_RUN_PATH=$LD_RUN_PATH:~/usr/lib
fi
export LD_RUN_PATH

# Locale
export LANGUAGE="en_US:en"
export LC_MESSAGES="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"

# openssl but 33919 work-around
export  OPENSSL_NO_TLS1_2=1

# Python / Gyp 64bit
export GYP_DEFINES=system_libdir=lib64

# Java
#The sun-java6 package is licensed software.
#You MUST read and agree to the license stored in
#/opt/sun-java6/LICENSE before using it.
#Use '. /opt/sun-java6/envsetup.sh' to setup environment.
[[ -f /opt/sun-java6/envsetup.sh ]] && . /opt/sun-java6/envsetup.sh

# Proxy enablement check
if [ -f ~/.bash_proxy ]; then
	. ~/.bash_proxy
else
	if [ -e "/var/tmp/proxy-mode" ]; then
		proxyon
	else
		proxyoff
	fi
fi

if [ -e /usr/share/terminfo/x/xterm-256color ]; then
        export TERM='xterm-256color'
else
        export TERM='xterm-color'
fi

